# seddit
Simple, fast and free alternative Reddit front-end.

## Installing
```
git clone https://gitlab.com/beucismis/seddit
cd seddit/
pip3 install -e .
```

## Running

```
flask --app seddit run
```

## Todo
- [ ] Formatting support for selftext
- [ ] Video and image support

## Preview

![](https://github.com/user-attachments/assets/386d8247-277e-4020-a09f-7e98f6faa661)

## License

`seddit` is distributed under the terms of the [WTPPL](LICENSE.txt) license.
