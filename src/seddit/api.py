from datetime import datetime
from dataclasses import dataclass

import requests


BASE_URL = "https://reddit.com"


@dataclass
class Submission:
    title: str
    ups: int
    stickied: bool
    selftext: str
    permalink: str
    created_utc: datetime


@dataclass
class Subreddit:
    subreddit_name_prefixed: str
    submissions: list[Submission]


class Reddit:
    def __init__(self, base_url: str = BASE_URL) -> None:
        self.base_url = base_url
        self.headers = {"user-agent": "seddit"}

    def _get(self, endpoint: str) -> dict:
        response = requests.get(f"{self.base_url}{endpoint}", headers=self.headers)

        if response.status_code != 200:
            pass

        return response.json()

    def subreddit(self, name: str, sort: str = None) -> Subreddit:
        submissions = []

        if sort is None:
            results = self._get(f"/r/{name}.json?limit=100")
        else:
            results = self._get(f"/r/{name}/{sort}.json?limit=100")

        childrens = results["data"]["children"]
        subreddit_name_prefixed = childrens[0]["data"]["subreddit_name_prefixed"]

        for children in childrens:
            submissions.append(
                Submission(
                    children["data"]["title"],
                    children["data"]["ups"],
                    children["data"]["stickied"],
                    children["data"]["selftext"],
                    "https://reddit.com/" + children["data"]["permalink"],
                    datetime.fromtimestamp(children["data"]["created_utc"]),
                )
            )

        return Subreddit(subreddit_name_prefixed, submissions)
