import flask

import seddit
from seddit.api import Reddit


reddit = Reddit()


@seddit.app.route("/", methods=["GET", "POST"])
def index():
    if flask.request.method == "POST":
        name = flask.request.form["name"]
        return flask.redirect(flask.url_for("subreddit", name=name))

    return flask.render_template("index.html")


@seddit.app.route("/r/<name>")
def subreddit(name: str):
    subreddit = reddit.subreddit(name=name)

    return flask.render_template("subreddit.html", subreddit=subreddit)


@seddit.app.route("/r/<name>/<sort>")
def subreddit_sort(name: str, sort: str):
    subreddit = reddit.subreddit(name=name, sort=sort)

    return flask.render_template("subreddit.html", subreddit=subreddit)


@seddit.app.errorhandler(404)
def page_not_found(e):
    return flask.render_template("404.html"), 404
