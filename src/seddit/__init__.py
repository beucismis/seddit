import logging

import flask


version = "1.0.0"

logging.basicConfig(level=logging.INFO)
app = flask.Flask(__name__)


import seddit.views
